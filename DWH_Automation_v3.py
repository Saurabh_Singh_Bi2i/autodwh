#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import pyodbc
import pandas as pd
import time
import sys
import csv


# In[2]:


print("####################################DWH PROJECT#########################################")


# In[3]:


#user_path = input("Enter the path of your configuration files: ")
user_path =sys.argv[1]


# In[4]:


#dim_name=input("\n\nPlease enter short name for which sql script is to be generated seperated by comma(,):")
dim_name =sys.argv[2]

# In[5]:


input_list = dim_name.split(',')
dim_names = [str(x.strip()) for x in input_list]


# In[6]:


#schema_name = input("\n\nEnter the schema name: ")
schema_name =sys.argv[3]

# In[7]:


#language_name = input("\n\nEnter the language name: ")
language_name =sys.argv[4]


# In[8]:


#fact_name =input("\n\nEnter the fact name: ")
fact_name =sys.argv[5]
fact_name=fact_name.replace("_"," ")

# In[9]:


#subj_area_name =input("\n\nEnter the subject area name: ")
subj_area_name =sys.argv[6]

# In[10]:


os.chdir(user_path)


# In[11]:


def config():
    separator = "="
    keys = {}
    with open('config.properties') as f:
        for line in f:
            if separator in line:
                name, value = line.split(separator, 1)
                keys[name.strip()] = value.strip()
    return keys


# In[12]:


def get_SQL_connection():
    conn = pyodbc.connect('Driver={'+config()['Driver']+'};Server='+config()['Server']+';Database='+config()['Database']+';')
    return conn


# In[13]:


def get_data(query):
    connect   = get_SQL_connection()
    cursor = connect.cursor()
    connect.autocommit=True
    cursor.execute(query)
    result=cursor.fetchall()
#     for row in result:
#         dimensions_code=row[0]
    return result
def create_table(query):
    connect   = get_SQL_connection()
    cursor = connect.cursor()
    connect.autocommit=True
    cursor.execute(query)
#    result=cursor.fetchall()

# In[14]:


def get_fact_data(query):
    connect   = get_SQL_connection()
    cursor = connect.cursor()
    connect.autocommit=True
    cursor.execute(query)
    rows=cursor.fetchall()
    result=[]
    while rows:
        result.append(rows)
        if cursor.nextset():
            rows = cursor.fetchall()
        else:
            rows = None
    return result


# In[15]:


def create_fact_scripts():
    fact_code=get_data("SELECT fact_code FROM [dwauto].fact_master where short_name ='"+fact_name+"'")[0][0]
    fact_script=get_fact_data("exec [dwauto].[fact_table_load_pr]    @in_fact_code ="+str(fact_code)+"     ,@in_schema_name ='"+schema_name+"'     ,@in_debug_flag='Y'    ,@in_create_merge_proc_flag = 'Y' ")
    fact_script1=fact_script[1][0][0]
    fact_script2=fact_script[2][0][0]
    load_script1 = open(user_path+'\\'+'load_'+fact_name+'.sql', "w")
    load_script2 = open(user_path+'\\'+'merge_'+fact_name+'.sql', "w")
    load_script1.write(fact_script1)
    load_script2.write(fact_script2)
    execute_script=create_table(fact_script1)
    execute_script=create_table(fact_script2)
    load_script1.close()
    load_script2.close()
    return load_script1,load_script2


# In[16]:


def create_dim_script(dimension_name):
    dimensions_code=get_data("SELECT dimension_code FROM [ABC].[dwauto].[dimension_master] where short_name='"+dimension_name+"'")[0][0]
    load_script2=get_data("exec [dwauto].[dim_table_create_pr]     @in_dimension_code = "+str(dimensions_code)+"     ,@in_schema_name ='"+schema_name+"'     ,@in_language_name = '"+language_name+"'     ,@in_debug_flag='Y'")
    load_script2=load_script2[0][0]
    load_script = open(user_path+'\\'+'create_dim_'+dimension_name+'.sql', "w")
    load_script.write(load_script2)
    #print(load_script2)
    execute_script=create_table(load_script2)
    load_script.close()
    return load_script
    


# In[17]:


def load_script(dimension_name):
    dimensions_code=get_data("SELECT dimension_code FROM [ABC].[dwauto].[dimension_master] where short_name='"+dimension_name+"'")[0][0]
    load_script2=get_data("exec [dwauto].[dim_table_load_for_star_schema_pr]     @in_dimension_code = "+str(dimensions_code)+"     ,@in_schema_name ='"+schema_name+"'     ,@in_language_name = '"+language_name+"'     ,@in_debug_flag='Y'")
    load_script2=load_script2[0][0]
    load_script = open(user_path+'\\'+'load_'+dimension_name+'.sql', "w")
    load_script.write(load_script2)
    execute_script=create_table(load_script2)
    load_script.close()
    return load_script
    


# In[42]:


def fact_create_script():
    sub_area_code=get_data("SELECT subjectarea_code FROM [ABC].[dwauto].[subjectarea_client_master] where short_name='"+subj_area_name+"'")[0][0]
    load_script2=get_data("exec [dwauto].[fact_table_create_pr]     @in_subjectarea_code = "+str(sub_area_code)+"     ,@in_schema_name ='"+schema_name+"'     ,@in_language_name = '"+language_name+"'     ,@in_debug_flag='Y'")
    load_script2=load_script2[0][0]
    load_script = open(user_path+'\\'+'create_fact_'+subj_area_name+'.sql', "w")
    load_script.write(load_script2)
    execute_script=create_table(load_script2)
    load_script.close()
    return load_script
    


# In[43]:


for names in dim_names:
    create_dim_script(names)


# In[44]:


for names in dim_names:
    load_script(names)


# In[45]:


create_fact_scripts()


# In[46]:


fact_create_script()


# In[23]:


print("\n\nAll the SQL scripts are generated successfully at "+user_path)


# In[24]:


print("#########################################THE END##########################################")


# In[ ]:


time.sleep(10)

